[![Testspace](https://www.testspace.com/img/Testspace.png)](https://www.testspace.com)

***

## Getting Started Examples 

Sample demonstrates techniques for using Testspace:
  * Using a Testspace Project that is `connected` with this Gitlab Repo
  * Using GitLab CI for demonstration purposes only
  * Refer to our [Getting Started](https://help.testspace.com/getting.started) help articles for more information

***

GitLab CI Example

For adding Testspace to your .gitlab-ci.yml refer to [add-to-ci](https://help.testspace.com/publish/push-data-add-to-ci#gitlab-ci)

`Published content` at https://samples.testspace.com/projects/testspace-samples:getting.started-gitlab.

[![pipeline status](https://gitlab.com/testspace-samples/getting.started-gitlab/badges/master/pipeline.svg)](https://gitlab.com/testspace-samples/getting.started-gitlab/commits/master)
[![Space Health](https://samples.testspace.com/spaces/74110/badge?token=9d4cdd68bc0820adfafcaaba9341e52f5490c9d2)](https://samples.testspace.com/spaces/74110 "Test Cases")
[![Space Metric](https://samples.testspace.com/spaces/74110/metrics/36744/badge?token=94669718518cbbaf490d7ae12f34fd8b103ac401)](https://samples.testspace.com/spaces/74110/schema/Code%20Coverage "Code Coverage (lines)")
[![Space Metric](https://samples.testspace.com/spaces/74110/metrics/36746/badge?token=973461c598ae87327b5e2d5b15b934a39d976072)](https://samples.testspace.com/spaces/74110/schema/Static%20Analysis "Static Analysis (issues)")


Download and configure the *Testspace client*: 

<pre>
curl -fsSL https://testspace-client.s3.amazonaws.com/testspace-linux.tgz | tar -zxvf- -C /usr/local/bin
testspace config url YOUR-subdomain.testspace.com
</pre>

Note that an *access-token* is NOT required for public repos.

Push content using *Testspace client*:
<pre>
testspace analysis.xml [tests]results*.xml coverage.xml 
</pre> 

